# -*- coding: utf-8 -*-

import os

import pytest

from wt.base import dict_to_object, process_list, Config, Content


@pytest.fixture(scope='function',
                params=[{'foo': 12}, {'foo': object()}, {'foo': True}],
                ids=['int', 'object', 'bool'])
def sample_dict(request):
    return request.param,dict_to_object(request.param)


@pytest.fixture(scope='function',
                params=[[{'foo': 12}, {'foo': object()}], [{'foo': True}]],
                ids=['list-1', 'list-2'])
def sample_list(request):
    return request.param,process_list(request.param)


@pytest.fixture(scope='function')
def sample_object():
    return dict_to_object({
        'foo': '${URL1}',
        'bar': {
            'baz': '${URL2}'
        },
        'boo': '${HOST}',
    })


@pytest.fixture(scope='function')
def config_factory():

    def factory(**kwargs):
        return Config(**kwargs)

    return factory


@pytest.fixture(scope='function')
def content(tmpdir):
    fn = tmpdir.mkdir('content').join('foo.md')
    fn.write('bar')
    return Content(src=str(fn))


@pytest.fixture(scope='function')
def content_without_src(tmpdir):
    return Content()


@pytest.fixture(scope='function')
def post(tmpdir):
    fn = tmpdir.mkdir('content').mkdir('posts').join('lorem.md')
    text = '\n'.join([
        '+++',
        'url = "/lorem/"',
        '+++',
        'ipsum',
    ])
    fn.write(text)
    return Content(src=str(fn))


@pytest.fixture(scope='function')
def page(tmpdir):
    fn = tmpdir.mkdir('content').mkdir('pages').join('ipsum.md')
    text = '\n'.join([
        '+++',
        'url = "/ipsum/"',
        '+++',
        'lorem',
    ])
    fn.write(text)
    return Content(src=str(fn))
